package com.malachane.github.data.api.model

import com.google.gson.annotations.SerializedName

data class APISearchRepo(

    @SerializedName("id")
    val id: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("language")
    val language: String,

    @SerializedName("forks_url")
    val forks: String,

    @SerializedName("watchers")
    val watchers: String,

    @SerializedName("license")
    val license: String,
)