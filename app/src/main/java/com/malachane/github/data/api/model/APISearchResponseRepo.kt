package com.malachane.github.data.api.model

import com.google.gson.annotations.SerializedName

data class APISearchResponseRepo (

    @SerializedName("items")
    val items: List<APISearchRepo>,
)