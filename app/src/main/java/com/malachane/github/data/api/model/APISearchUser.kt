package com.malachane.github.data.api.model

import com.google.gson.annotations.SerializedName

data class APISearchUser (
    @SerializedName("id")
    val id: String,

    @SerializedName("login")
    val login: String,

    @SerializedName("avatar_url")
    val avatar: String,
)