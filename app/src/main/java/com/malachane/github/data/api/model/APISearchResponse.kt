package com.malachane.github.data.api.model

import com.google.gson.annotations.SerializedName

data class APISearchResponse (

    @SerializedName("items")
    val items: List<APISearchUser>,
)