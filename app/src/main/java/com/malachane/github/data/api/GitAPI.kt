package com.malachane.github.data.api

import com.malachane.github.data.api.model.APISearchResponse
import com.malachane.github.data.api.model.APISearchResponseRepo
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitAPI {

    companion object {
        const val BASE_URL = "https://api.github.com"
    }


    @GET("/search/users")
    suspend fun searchUsers(
        @Query("q") search: String
    ): APISearchResponse

    @GET("/users/{user}/repos")
    suspend fun searchRepos(
        @Path("user") search: String
    ): APISearchResponseRepo
}