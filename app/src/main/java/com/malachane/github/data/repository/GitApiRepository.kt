package com.malachane.github.data.repository

import com.malachane.github.data.api.GitAPI
import com.malachane.github.data.api.model.APISearchRepo
import com.malachane.github.data.api.model.APISearchUser
import com.malachane.github.domain.model.GitRepos
import com.malachane.github.domain.model.GitUser
import com.malachane.github.domain.repository.GitRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class GitApiRepository : GitRepository {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(GitAPI.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val api = retrofit.create(GitAPI::class.java)

    override suspend fun searchUsers(search: String): List<GitUser> {
        return api.searchUsers(search).items.map {
            it.toUser()
        }
    }

    override suspend fun searchRepos(search: String): List<GitRepos> {
        return api.searchRepos(search).items.map {
            it.toRepo()
        }

    }
}

private fun APISearchUser.toUser() =
    GitUser(
        this.id,
        this.login,
        this.avatar
    )

private fun APISearchRepo.toRepo() =
    GitRepos(
        this.name,
        this.description,
        this.language,
        this.forks,
        this.watchers,
        this.license
    )
