package com.malachane.github.domain.model

import com.google.gson.annotations.SerializedName

data class GitUser(
    val id: String,

    val login: String,

    val avatar: String,
)