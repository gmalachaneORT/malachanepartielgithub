package com.malachane.github.domain.repository

import com.malachane.github.domain.model.GitRepos
import com.malachane.github.domain.model.GitUser

interface GitRepository {
    suspend fun searchUsers(search: String): List<GitUser>
    suspend fun searchRepos(search: String): List<GitRepos>
}