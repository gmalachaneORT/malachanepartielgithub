package com.malachane.github.domain.model

import com.google.gson.annotations.SerializedName

data class GitRepos(

    val id: String,

    val name: String,

    val description: String,

    val language: String,

    val forks: String,

    val watchers: String,

    val license: String,
)
