package com.malachane.github.presentation.repos

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.malachane.github.data.repository.GitApiRepository
import com.malachane.github.domain.repository.GitRepository
import com.malachane.github.presentation.search.SearchState
import kotlinx.coroutines.launch

class SearchRepoViewModel(application: Application) : AndroidViewModel(application) {

    private val gitRepository: GitRepository = GitApiRepository()

    private val _state = MutableLiveData<SearchState>()

    val state: LiveData<SearchState> get() = _state

    fun searchRepos(text: String) {
        _state.value = SearchState.Loading

        viewModelScope.launch {
            try {
                _state.value = SearchState.Success(gitRepository.searchUsers(text))
            } catch (e: Exception) {
                Log.e("BUG", "Exception $e")
                _state.value = SearchState.Error
            }
        }

    }
}