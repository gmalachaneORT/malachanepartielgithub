package com.malachane.github.presentation

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.malachane.github.R
import com.malachane.github.domain.model.GitUser
import com.squareup.picasso.Picasso

class GitUserAdapter(
    context: Context,
    private val listener: OnUserClickListener
) : RecyclerView.Adapter<GitUserAdapter.ViewHolder>() {

    interface OnUserClickListener {
        fun onUserClicked(id: String)
    }

    private val users: ArrayList<GitUser> = ArrayList()

    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.item_users, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(users[position])
    }

    override fun getItemCount() = users.size

    @SuppressLint("NotifyDataSetChanged")
    fun setUsers(data: List<GitUser>?) {
        users.clear()

        data?.let {
            users.addAll(data)
        }

        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val login: TextView = view.findViewById(R.id.user_name)
        private val avatar: ImageView = view.findViewById(R.id.user_avatar)

        init {
            view.setOnClickListener {
                listener.onUserClicked(users[adapterPosition].id)
            }
        }

        fun bind(gitUser: GitUser) {
            login.text = gitUser.login

            Picasso.get().load(gitUser.avatar).into(avatar)
        }
    }

}
