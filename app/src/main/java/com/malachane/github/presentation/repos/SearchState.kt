package com.malachane.github.presentation.repos

import com.malachane.github.domain.model.GitRepos
import com.malachane.github.domain.model.GitUser

sealed class SearchState {

    class Success(val gitRepos: List<GitRepos>) : SearchState()

    object Loading : SearchState()

    object Error : SearchState()

}