package com.malachane.github.presentation

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.malachane.github.R
import com.malachane.github.domain.model.GitRepos
import com.malachane.github.domain.model.GitUser
import com.squareup.picasso.Picasso

class GitRepoAdapter(
    context: Context,
    private val listener: GitUserAdapter.OnUserClickListener
) : RecyclerView.Adapter<GitRepoAdapter.ViewHolder>() {


    private val repos: ArrayList<GitRepos> = ArrayList()

    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.item_repos, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(repos[position])
    }

    override fun getItemCount() = repos.size

    @SuppressLint("NotifyDataSetChanged")
    fun setRepo(data: List<GitRepos>?) {
        repos.clear()

        data?.let {
            repos.addAll(data)
        }

        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val name: TextView = view.findViewById(R.id.repos_name)
        private val language: TextView = view.findViewById(R.id.repos_language)
        private val description: TextView = view.findViewById(R.id.repos_description)
        private val forks: TextView = view.findViewById(R.id.repos_forks)
        private val watchers: TextView = view.findViewById(R.id.repos_watcher)
        private val license: TextView = view.findViewById(R.id.repos_licence)

        init {
            view.setOnClickListener {
                listener.onUserClicked(repos[adapterPosition].id)
            }
        }

        fun bind(gitRepos: GitRepos) {
            name.text = gitRepos.name
            language.text = gitRepos.language
            description.text = gitRepos.description
            forks.text = gitRepos.forks
            watchers.text = gitRepos.watchers
            license.text = gitRepos.license
        }
    }

}