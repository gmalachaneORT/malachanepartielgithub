package com.malachane.github.presentation.repos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.malachane.github.R
import com.malachane.github.presentation.GitRepoAdapter
import com.malachane.github.presentation.GitUserAdapter
import com.malachane.github.presentation.search.SearchFragment
import com.malachane.github.presentation.repos.SearchState
import com.malachane.github.presentation.search.SearchViewModel

class SearchRepoFragment : Fragment(), GitUserAdapter.OnUserClickListener {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private lateinit var name: TextView
    private lateinit var description: TextView
    private lateinit var language: TextView
    private lateinit var forks: TextView
    private lateinit var watchers: TextView
    private lateinit var license: TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var progress: ProgressBar

    private lateinit var adapter: GitRepoAdapter

    private val viewModel: SearchViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        name = view.findViewById(R.id.repos_name)
        language = view.findViewById(R.id.repos_language)
        description = view.findViewById(R.id.repos_description)
        forks = view.findViewById(R.id.repos_forks)
        watchers = view.findViewById(R.id.repos_watcher)
        license = view.findViewById(R.id.repos_licence)
        progress = view.findViewById(R.id.fragment_repos_progress)
        recyclerView = view.findViewById(R.id.fragment_repos_recyclerview)

        adapter = GitRepoAdapter(requireContext(), this)

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter



        viewModel.state.observe(viewLifecycleOwner, ::onStateChanged)
    }

    private fun onStateChanged(state: SearchState) {
        when (state) {
            is SearchState.Loading -> {

                progress.visibility = View.VISIBLE

                adapter.setRepo(null)
            }
            is SearchState.Success -> {

                progress.visibility = View.GONE

                adapter.setRepo(state.gitRepos)
            }
            is SearchState.Error -> {
                progress.visibility = View.GONE

                adapter.setRepo(null)

                Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onUserClicked(id: String) {
    }

}