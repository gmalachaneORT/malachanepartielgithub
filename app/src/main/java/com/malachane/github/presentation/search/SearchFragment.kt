package com.malachane.github.presentation.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.malachane.github.R
import com.malachane.github.presentation.GitUserAdapter
import com.malachane.github.presentation.MainActivity

class SearchFragment : Fragment(), GitUserAdapter.OnUserClickListener {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private lateinit var editText: EditText
    private lateinit var button: Button
    private lateinit var recyclerView: RecyclerView
    private lateinit var progress: ProgressBar

    private lateinit var adapter: GitUserAdapter

    private val viewModel: SearchViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        editText = view.findViewById(R.id.fragment_search_edittext)
        progress = view.findViewById(R.id.fragment_search_progress)
        recyclerView = view.findViewById(R.id.fragment_search_recyclerview)
        button = view.findViewById(R.id.fragment_search_button)

        adapter = GitUserAdapter(requireContext(), this)

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter

        button.setOnClickListener {
            viewModel.searchUsers(editText.text.toString())
        }

        viewModel.state.observe(viewLifecycleOwner, ::onStateChanged)
    }

    private fun onStateChanged(state: SearchState) {
        when (state) {
            is SearchState.Loading -> {

                progress.visibility = View.VISIBLE

                adapter.setUsers(null)
            }
            is SearchState.Success -> {

                progress.visibility = View.GONE

                adapter.setUsers(state.gitUsers)
            }
            is SearchState.Error -> {
                progress.visibility = View.GONE

                adapter.setUsers(null)

                Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onUserClicked(id: String) {
    }

}