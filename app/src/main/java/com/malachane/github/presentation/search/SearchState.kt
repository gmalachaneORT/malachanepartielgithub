package com.malachane.github.presentation.search

import com.malachane.github.domain.model.GitUser

sealed class SearchState {

    class Success(val gitUsers: List<GitUser>) : SearchState()

    object Loading : SearchState()

    object Error : SearchState()

}